# Description

`typos-git-commit` is a command line tool that processes a [`typos`](https://crates.io/crates/typos-cli)
generated json file in order to :

1. correct typos in detected files (via sed)
2. make a commit for each correction (via a simple git commit command)
   grouping every affected file with that correction in a single commit.

It has been written for a one shot project but one may find it to
be useful. Use `typos-git-commit --help` to get usage help.

# Usage

You can use the option `--noop` to print what will be executed without
effectively execute anything.

If you need to have a summary of all typos you can use `--only-list-typos`
option. You can add some details to this summary with `--details` option.

You can filter out what will be done using `--exclude-file`, `--exclude-typo`
and `--exclude-correction` (options may be invoked more than once) to
exclude a specific file, typo or correction from being taken into account.

Follow these steps to correct the typos found in all file but Python
and Javascript scripts of a project:

* Generate a json file with `typos`, for instance:
  `typos doc/ --exclude *.py --exclude *.js --format json >typos.json`
* In the directory of the repository use `typos-git-commit` :
  `typos-git-commit --filename typos.json`.
  This command will correct only typos with more than 5 (by default)
  characters and the ones that got only one correction proposal. To
  modify the minimum length one may use `--minlen`.
* Review carefully the generated commits even if everything is done to
  mitigate errors when replacing typos there are situations where this
  correction is really an error.
* `--message` option allows to change your commit message. One may use
  the special notations {typo} and {correction} that will be replaced
  respectively by the typo to be corrected and the correction of that
  typo. Tip: use `--noop` to see how the git message will be before
  applying.

# Completion

The command has no short version arguments except for `--help` (`-h`)
and `--version` (`-V`) thus to ease writing arguments at command
line typos-git-commit can generate shell's completion scripts with
`--completion bash` option (see help to get all supported shells).

* Bash completion script is generated using `typos-git-commit --completion
  bash`. You can add this completion script to your local profile using
  these commands:
  ```
  mkdir -p ~/.local/share/bash-completion/completions
  typos-git-commit --completion bash > ~/.local/share/bash-completion/completions/typos-git-commit
  ```

# Changelog

See [ChangeLog](ChangeLog) file or use `typos-git-commit --changelog`
