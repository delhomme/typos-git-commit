/* -*- coding: utf8 -*-
 *
 *  main.rs : Get the file to process and makes commit in the repository
 *
 *  (C) Copyright 2022 - 2024 Olivier Delhomme
 *  e-mail : olivier.delhomme@free.fr
 *
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3, or (at your option)
 *  any later version.
 *
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program; if not, write to the Free Software Foundation,
 *  Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307, USA.
 */
#![doc = include_str!("../README.md")]
#![forbid(unsafe_code)]

use std::process::exit;
use typos_git_commit::cli::Cli;
use typos_git_commit::thashmap::THashMap;

fn main() {
    let cli = Cli::analyze();

    let thm = THashMap::new();
    let thm = match thm.read_typos_file(&cli) {
        Ok(t) => t,
        Err(e) => {
            eprintln!("Error: {e}");
            exit(1);
        }
    };

    if cli.only_list_typos {
        thm.list_typos(&cli);
    } else {
        thm.correct_typos(&cli);
        if !cli.noop {
            println!("Please review the commits before submitting !");
        }
    }
}
