use crate::cli::Cli;
use serde::Deserialize;

// A typical typo line in JSON format from typos output program:
// {"type":"typo","path":"doc/man/man5/burst_buffer.conf.5","line_num":65,"byte_offset":20,"typo":"preceeded","corrections":["preceded","proceeded"]}

#[derive(Deserialize, Debug, Clone)]
pub struct TyposJsonLine {
    #[serde(default, rename = "type")]
    pub type_id: String,

    #[serde(default)]
    pub path: String,

    #[serde(default)]
    pub line_num: u32,

    #[serde(default)]
    pub byte_offset: u32,

    #[serde(default)]
    pub typo: String,

    #[serde(default)]
    pub corrections: Vec<String>,
}

impl TyposJsonLine {
    // Tells whether the file has been excluded from typos corrections
    pub fn is_file_excluded(&self, cli: &Cli) -> bool {
        match &cli.exclude_file {
            Some(list) => {
                for file in list {
                    if self.path.contains(file) {
                        return true;
                    }
                }
                false
            }
            None => false,
        }
    }

    // Tells whether typo contains one excluded typo
    pub fn is_typo_excluded(&self, cli: &Cli) -> bool {
        match &cli.exclude_typo {
            Some(list) => {
                for t in list {
                    if self.typo.contains(t) {
                        return true;
                    }
                }
                false
            }
            None => false,
        }
    }

    // Tells whether typo contains one excluded correction
    pub fn is_correction_excluded(&self, cli: &Cli) -> bool {
        match &cli.exclude_correction {
            Some(list) => {
                for t in list {
                    for correction in &self.corrections {
                        if correction.contains(t) {
                            return true;
                        }
                    }
                }
                false
            }
            None => false,
        }
    }

    // A TypoJsonLine is excluded if at least one of typo, correction or file is excluded
    // from being corrected
    pub fn is_excluded(&self, cli: &Cli) -> bool {
        self.is_typo_excluded(cli) || self.is_correction_excluded(cli) || self.is_file_excluded(cli)
    }
}
